<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:CPRDetails="CPRDetails" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="yes" method="xml"/>
	<!-- Copy elements without copying their namespace declarations -->
  <xsl:template name="identity" match="*">
    <xsl:element name="{name()}">
			<xsl:apply-templates select="node()|@*"/>
		</xsl:element>
	</xsl:template>
	<!-- Identity template for empty elements -->
	<!-- Hack because of the Client's requirement -->
	<!-- Client requires a pair of start and end tags (<empty></empty>) instead of self-closing tag (<empty />). -->
	<xsl:variable name="empty" select="''"/>
  <xsl:template match="*[not(node())]">
    <xsl:element name="{name()}">
			<xsl:apply-templates select="node()|@*"/>
			<xsl:value-of select="$empty"/>
		</xsl:element>
	</xsl:template>
	<!-- Drop unwanted elements, but keep their children -->
  <xsl:template match="CPRDetails:Report">
		<xsl:apply-templates/>
	</xsl:template>
  <xsl:template match="CPRDetails:APINVOICE_Collection">
		<xsl:apply-templates/>
	</xsl:template>
</xsl:stylesheet>