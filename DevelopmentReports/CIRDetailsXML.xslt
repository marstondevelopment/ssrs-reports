<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:CIRDetailsXML="CIRDetailsXML">
  <xsl:output method="xml"
              indent="yes"/>

  <!-- Copy elements without copying their namespace declarations -->
  <xsl:template match="*"
                name="identity">
    <xsl:element name="{name()}">
      <xsl:apply-templates select="node()|@*"/>
    </xsl:element>
  </xsl:template>

  <!-- Identity template for empty elements -->
  <!-- Hack because of the Client's requirement -->
  <!-- Client requires a pair of start and end tags (<empty></empty>) instead of self-closing tag (<empty />). -->
  <xsl:variable name="empty"
                select="''"/>
  <xsl:template match="*[not(node())]">
    <xsl:element name="{name()}">
      <xsl:apply-templates select="node()|@*"/>
      <xsl:value-of select="$empty"/>
    </xsl:element>
  </xsl:template>

  <!-- Drop unwanted elements, but keep their children -->
  <xsl:template match="CIRDetailsXML:Report">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="CIRDetailsXML:APINVOICE_Collection">
    <xsl:apply-templates/>
  </xsl:template>

</xsl:stylesheet>